const jwt = require('jsonwebtoken')

const getAboutUs = async (req, res) =>{
  const tokenCookies = req.cookies.token
  let decryptedToken = null
  if (tokenCookies) {
      // cek token 
      decryptedToken = jwt.verify(tokenCookies, 'hahaha')
  }
  res.status(200).render('aboutUs', {
      user:decryptedToken
  })
}

module.exports = {
    getAboutUs
}