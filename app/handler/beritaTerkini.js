const { berita } = require('../model/berita')
// const { hashtag_berita } = require('../model/hashtag_berita')
const { hashtag } = require('../model/hashtag')
const { hashtag_berita } = require('../model/hashtag_berita')
const {Pagination} = require('../utils/helper')
const jwt = require('jsonwebtoken')

const getBeritaTerkini = async (req,res) =>{

  try { 
      const tokenCookies = req.cookies.token
      let decryptedToken = null
      if (tokenCookies) {
          // cek token 
          decryptedToken = jwt.verify(tokenCookies, 'hahaha')
      }
        let dataMaster = []
        const beritaItem = await berita.findAll()
        const hashtagItem = await hashtag.findAll()  
        const hb =  await hashtag_berita.findAll()

        for (let i = 0; i < beritaItem.length; i++) {
            beritaItem[i].dataValues.hashtag = []
           for (let j = 0; j < hb.length; j++) {
             let tags = hashtagItem.find(tag => tag.id == hb[j].id_hashtag && hb[j].id_berita == beritaItem[i].dataValues.id)
             if (tags) {
                 beritaItem[i].dataValues.hashtag.push(tags)
             }
           }
           dataMaster.push(beritaItem[i])
        }
        
        
        const dataLoaded = Pagination(dataMaster,req.query.page)
        // console.log(dataMaster.dataValues)
        res.status(200).render('berita-terkini', {
            dataLoaded,
            user:decryptedToken
        })
        // res.status(200).send(dataMaster[0].dataValues.hashtag)
    } catch (error) {
        console.log(error)
        res.status(500).json({
            message:'ini error',
            error
        })
      }
    }
    

  
module.exports = {
    getBeritaTerkini
}