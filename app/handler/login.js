const bcryptjs = require('bcryptjs')
const jwt = require('jsonwebtoken')
const {
  user
} = require('../model/user')


const login = async function (req, res) {
  try {
    let email = req.body.email
    let password = req.body.password

    const userLogin = await user.findOne({
      where: {
        email: email,
        isBlocked: false
      }
    })

    if (!userLogin) {
      res.status(200).render('login_user', {
        message: "anda tidak bisa akses untuk sementara waktu",
        user: null
      })
      return
    }

    const isPasswordTrue = bcryptjs.compareSync(password, userLogin.password)

    if (isPasswordTrue === false) {
      res.status(200).render('login_user', {
        message: "Password salah",
        user: null
      })
      return
    }
    const token = jwt.sign({
      username:userLogin.username,
      id: userLogin.id
      }, 'hahaha')
      res.cookie( 'token', token)
      res.redirect('/')
    
    // res.status(200).send(dataMaster)
  } catch (error) {
    console.log(error)
    res.status(500).json({
      message: 'ini error',
      error
    })
  }
}

const viewLogin = async function (req, res) {
  res.status(200).render('login_user', {
    user: null
  })
  return
}

const logout = async function (req, res){
  const token = req.cookies.token
  res.clearCookie('token');
  res.redirect('/')
}


module.exports = {
  login,
  viewLogin,
  logout
}


// delsoon 

// res.status(200).render('homepage', {
    //   message: "Login berhasil",
    //   user: userLogin,
    //   carousel,
    //   datas: dataMaster,
    // })
    // return
    // res.status(200).render('homepage', {
    //     datas:dataMaster,
    //     carousel
    // })