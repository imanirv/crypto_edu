const jwt = require('jsonwebtoken')

const getWatchList = async (req, res) =>{

        const tokenCookies = req.cookies.token
        let decryptedToken = null
        if (tokenCookies) {
            // cek token 
            decryptedToken = jwt.verify(tokenCookies, 'hahaha')
        }

    res.status(200).render('watchlist', {
        user:decryptedToken
    })
 
}

module.exports = {
    getWatchList
}

  // add auth 
  // const token = req.get("authorization") || null
  // let userLogin = null
  // if(token !== null) {
  //   userLogin = await user.findOne({
  //     where: {
  //       id: token
  //     }
  //   })
  //   res.set('authorization', userLogin.id);
  // }
  // add auth end