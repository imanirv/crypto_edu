const express = require('express');
const router = express.Router({caseSensitive: true});
const handler = require('../handler')

// router.get('/', (req, res) => {
//     res.json({
//         success: true
//     })
// })



// router.get('/user/:nama', handler.user.showNamaUser)

// router.get('/user', handler.user.showUser)

// router.post('/user/create', handler.user.create)
// router.put('/user/update/:id', handler.user.update)
// router.get('/user/delete/:id', handler.user.deleted)

// berita terkini 
router.get('/berita-terkini', handler.BeritaTerkini.getBeritaTerkini)


//homepage
router.get('/', handler.homepage.viewAllBerita)

// Register
router.get('/user/register', handler.register_user.showRegister)
router.post('/user/register', handler.register_user.register)

//Detail Berita
router.get('/berita/:slug', handler.detail_berita.showDetail)
router.post('/berita/submit-komentar', handler.detail_berita.submitKomen)



//Kategori berita
router.get('/kategori/:nama_kategori', handler.kategori_berita.showBerita)

//Sitemap
router.get('/sitemap', handler.sitemap.getSitemap);

router.get('/about-us', handler.aboutUs.getAboutUs);

//Login Admin
router.get('/admin', handler.admin.adminPage)
router.post('/admin/add', handler.admin.addAdmin) //untuk menambahkan admin melalui postman atau insomnia
router.get('/admin/login', handler.admin.showLoginAdmin)
router.post('/admin/login', handler.admin.loginAdmin)

router.post('/user/login', handler.auth.login)
router.get('/user/login', handler.auth.viewLogin)
// Edit profile user
router.get('/user/edit-profile/:username', handler.edit_profile_user.showProfileUser)
router.post('/user/edit-profile/:username', handler.edit_profile_user.updateProfileUser)

// Edit password pengguna
router.get('/user/edit-password/:username', handler.edit_password_user.showPasswordUser)
router.post('/user/edit-password/:username', handler.edit_password_user.updatePasswordUser)
//Admin Pengaturan Komentar
router.get('/admin/komentar', handler.admin_komentar.showKomentar)
router.get('/admin/komentar/delete/:id', handler.admin_komentar.deleteKomentar)

// admin 
router.put('/admin/block-users', handler.admin.blockingUser)
router.get('/admin/block-users', handler.admin.getDataBlocking)
//Admin Dashboard
//CRUD Kategori Berita
router.get('/admin/kategori', handler.admin.showAllKategori)
router.post('/admin/kategori', handler.admin.createKategori)
router.get('/admin/kategori/ubah/:id', handler.admin.formUpdateKategori)
router.post('/admin/kategori/ubah/:id', handler.admin.editKategori)
router.get('/admin/kategori/hapus/:id', handler.admin.deleteKategori)
//CRUD Hashtag Berita
router.get('/admin/hashtag', handler.admin.showAllHashtag)
router.post('/admin/hashtag', handler.admin.createHashtag)
router.get('/admin/hashtag/ubah/:id', handler.admin.formUpdateHashtag)
router.post('/admin/hashtag/ubah/:id', handler.admin.editHashtag)
router.get('/admin/hashtag/hapus/:id', handler.admin.deleteHashtag)
//admin-berita
router.get('/admin/showBerita', handler.admin_berita.showBerita)
router.get('/admin/buat-berita', handler.admin_berita.showCreateBerita)
router.post('/admin/buat-berita', handler.admin_berita.create)
router.get('/admin/delete/:id', handler.admin_berita.hapus)
router.post('/admin/update/:id', handler.admin_berita.update)
router.get('/admin/update/:id', handler.admin_berita.showUpdate)
//halaman hashtag
router.get('/hashtag/:nama_tag', handler.hashtag_berita.showHashtag);
//Admin Pengaturan Komentar
router.get('/admin/komentar', handler.admin_komentar.showKomentar)
router.get('/admin/komentar/delete/:id', handler.admin_komentar.deleteKomentar)
//watchlist
router.get('/watchlist', handler.watchlist.getWatchList);

// logout 
router.get('/logout', handler.auth.logout);
module.exports = router;
